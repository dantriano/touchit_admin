import { Component } from '@angular/core';

@Component({
  selector: 'ngx-settings',
  template: `<lib-breadcrumbs></lib-breadcrumbs><router-outlet></router-outlet>`,
})
export class SettingsComponent {}

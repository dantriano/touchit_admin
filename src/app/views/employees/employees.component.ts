import { Component } from '@angular/core';

@Component({
  selector: 'ngx-employees',
  template: `<lib-breadcrumbs></lib-breadcrumbs><router-outlet></router-outlet>`,
})
export class EmployeesComponent {
}

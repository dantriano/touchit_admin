## [Touch It](./README.md) version `changelog`

###### `v0.1`
- chore: update to `Angular 9.0.0`
    - [https://update.angular.io/](https://update.angular.io/#8.0:9.0)
    - [https://angular.io/guide/updating-to-version-9](https://angular.io/guide/updating-to-version-9)
    - [https://blog.angular.io/version-9-of-angular-now-available-project-ivy-has-arrived](https://blog.angular.io/version-9-of-angular-now-available-project-ivy-has-arrived-23c97b63cfa3)

- chore: update to `TypeScript 3.7`
    - [https://www.typescriptlang.org/docs/handbook/release-notes/typescript-3-7.html](https://www.typescriptlang.org/docs/handbook/release-notes/typescript-3-7.html)
    - [https://www.typescriptlang.org/docs/handbook/release-notes/typescript-3-6.html](https://www.typescriptlang.org/docs/handbook/release-notes/typescript-3-6.html)

###### dependencies update
- update `@angular/animations` to `^9.0.0`
- update `@angular/common` to `^9.0.0`
- update `@angular/compiler` to `^9.0.0`
- update `@angular/core` to `^9.0.0`
- update `@angular/forms` to `^9.0.0`
- update `@angular/platform-browser` to `^9.0.0`
- update `@angular/platform-browser-dynamic` to `^9.0.0`
- update `@angular/router` to `^9.0.0`
- update `@coreui/angular` to `^2.9.0`
- update `flag-icon-css` to `^3.4.6`
- update `@angular-devkit/build-angular` to `^0.900.1`
- update `@angular/cli` to `^9.0.1`
- update `@angular/compiler-cli` to `^9.0.0`
- update `@angular/language-service` to `^9.0.0`
- update `@types/jasmine` to `^3.5.3`
- update `karma-jasmine-html-reporter` to `^1.5.2`
- update `typescript` to `~3.7.5`
- feature: Activities section
- feature: Employees section
- feature: Locations section
- feature: Use CoreUI for Angular as base
